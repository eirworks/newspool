@extends('layout')

@section('content')
    @guest
        <div class="alert alert-info">
            {{ __('home.welcome', ['app_name' => env('APP_NAME')]) }}
            <a href="{{ route('login') }}">@lang('auth.login')</a>
        </div>
    @endguest

    @if($news_items->count() > 0)
        @if(request()->filled('q'))
            <p>
                We found {{ $news_items->count() }} results
                of <strong>{{ request()->input('q') }}</strong>
            </p>
        @endif
        <ul class="news-items">
            @foreach($news_items as $news_item)
                <li class="news-item">
                    <div class="row">
                        <div class="col-md-10">

                            <strong><a href="{{ route('go',[hashid_encode($news_item->id), 'u' => $news_item->url_hash]) }}" target="_blank">{{ $news_item->title }}</a></strong>
                            @if(!empty($news_item->source))
                                <span class="label label-primary">{{ $news_item->source }}</span>
                            @endif
                            <div class="news-item-url"><small>{{ $news_item->url }}</small></div>
                            <div class="news-item-content">
                                {!! $news_item->content_html !!}
                            </div>
                            <div class="news-item-meta">
                                <a href="{{ url('/item/'.hashid_encode($news_item->id)) }}">Comments</a>
                                &mdash; Author:
                                @if($news_item->author)
                                    <span>{{ $news_item->author }}</span>
                                @else
                                    {{ $news_item->source }} Reporter
                                @endif
                                &mdash;
                                Published:
                                {{ $news_item->created_at }}
                                @if(env('APP_DEBUG'))
                                    <code>{{ $news_item->url_hash }}</code>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            @if($news_item->thumbnail_image)
                                <img src="{{ $news_item->thumbnail_image }}" alt="Thumbnail of '{{ $news_item->title }}'" class="news-item-thumbnail">
                            @endif
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>

        {!! $news_items->appends([
            'q' => request()->input('q'),
            'source' => request()->input('source'),
            'content' => request()->input('content'),
        ])->links() !!}
    @else
        <div class="fail">
            <p>
                Can't find anything here...
            </p>
        </div>
    @endif
    <p class="text-center">
        We have {{ $items }} news items
        from {{ $feeds }} sources.
    </p>
@endsection
