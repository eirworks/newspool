<?php

return [
    'roles' => [
        // Power
        'superadmin',

        // News related
        'add_news','edit_news','delete_news','toggle_hide_news',

        // Topic
        'add_topic', 'edit_topic', 'delete_topic',
    ],
];