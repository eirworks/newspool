@extends('layout')

@section('title', "Redirecting...")
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="text-center">
                <p class="lead">
                    You are being redirected to:
                    <code>{{ $news_item->url }}</code>
                </p>
                <p>
                    <small>
                        <a href="{{ $news_item->url }}">Click here if you're not being redirected...</a>
                    </small>
                </p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){
            window.location.replace("{!! trim($news_item->url) !!}")
        }, 3000);
    </script>
@endsection
