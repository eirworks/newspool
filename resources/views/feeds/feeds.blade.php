@extends('layout')

@section('title')
News Sources
@endsection

@section('content')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('feeds::feeds') }}">Sources</a></li>
    </ul>

    <p>
        <strong>What is News Sources?</strong>
        News source is feed urls that will be used by
        our crawlers to collect news.
        @auth
            <a href="{{ route('feeds::new') }}" class="btn btn-primary">Add Feed</a>
        @endauth
    </p>

    @if($feeds->count() > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Channel</th>
                    <th>Source</th>
                    <th>Submitter</th>
                    <th>Status</th>
                    @auth
                        <th>&nbsp;</th>
                    @endauth
                </tr>
            </thead>
            <tbody>
                @foreach($feeds as $feed)
                    <tr>
                        <td>{{ $feed->id }}</td>
                        <td><a href="{{ route('home', ['source' => $feed->channel]) }}"><span class="label label-primary">{{ $feed->channel }}</span></a></td>
                        <td>
                            @if($feed->name)
                                {{ $feed->name }}
                                &mdash;
                            @endif
                            <a href="{{ $feed->url }}">{{ $feed->url }}</a>
                        </td>
                        <td>
                            {{ $feed->user->name }}
                        </td>
                        <td>
                            @if($feed->fail)
                                <span class="label label-danger">Failing</span>
                            @else
                                <span class="label label-success">OK</span>
                            @endif
                        </td>
                        @auth
                        <td>
                            <a href="{{ route('feeds::delete',[$feed]) }}" class="text-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                        @endauth
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $feeds->links() !!}
        @auth
            <div class="btn-group btn-group-sm">
                <a href="{{ route('feeds::backup') }}" class="btn btn-default">Backup Sources</a>
                @if(\Storage::exists(\App\Http\Controllers\Feeds\FeedController::BACKUP_FILE))
                    <a href="{{ route('feeds::restore') }}" class="btn btn-default">Restore</a>
                    <a href="{{ route('feeds::backup::view') }}" class="btn btn-default">View Backup</a>
                    <a href="{{ route('feeds::backup::view',['download' => 1]) }}" class="btn btn-default">Download Backup</a>
                @endif
            </div>
        @endauth
    @else
        <div class="fail">
            <p>
                No news source available right now.
            </p>
        </div>
    @endif
@endsection

