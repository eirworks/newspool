@extends('layout')

@section('title', 'Add Feed')

@section('content')
    <form action="{{ route('feeds::save') }}" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>URL</label>
            <input type="text" name="url" value="" placeholder="URL of the Feed" class="form-control">
        </div>

        <div class="form-group">
            <label>Name (optional)</label>
            <input type="text" name="name" value="" placeholder="Feed Name" class="form-control">
        </div>

        <div class="form-group">
            <label>Channel/Source (optional)</label>
            <input type="text" name="channel" value="" placeholder="Feed Name" class="form-control">
            <div class="help-block">Example: BBC, CNN, New York Times, Guardian, etc.</div>
        </div>

        <p>
            <button class="btn btn-primary">Submit</button>
        </p>
    </form>
@endsection

