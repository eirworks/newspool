<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model
{
    const TYPE_NEWS = 0;
    const TYPE_ARTICLE = 1; // like news but not newsworthy
    const TYPE_IMAGE = 2; // will preview the image
    const TYPE_VIDEO = 3; // will preview thumbnail

    protected $attributes = [
        'image' => "",
        'thumbnail_image' => "",
        "content_html" => "",
        "content_md" => "",
        "source" => "?",
    ];

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function topics()
    {
        return $this->belongsToMany('App\NewsTopic', 'news_topic_relations', 'news_item_id', 'news_topic_id');
    }
}
