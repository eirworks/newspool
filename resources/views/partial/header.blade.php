<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">{{ env('APP_NAME') }}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            {{--<li class="active"><a href="#">Link</a></li>--}}
{{--            @auth<li><a href="{{ route('news::add') }}">Add News</a></li>@endauth--}}
        </ul>
        <form class="navbar-form navbar-left" role="search" action="{{ route('home') }}" method="get">
            <div class="form-group">
                <input type="text" name="q" value="{{ request()->input('q') }}" class="form-control" placeholder="{{ __('home.search_news') }}">
            </div>
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            <a href="{{ route('search', [
                'q' => request()->input('q'),
                'source' => request()->input('source'),
                'content' => request()->input('content'),
            ]) }}" class="btn btn-sm btn-link">@lang('home.advanced_search_news')</a>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ route('feeds::feeds') }}">Sources</a></li>
            @guest
                <li><a href="{{ route('login') }}">@lang('auth.login')</a></li>
                <li><a href="{{ route('register') }}">@lang('auth.register')</a></li>
            @endguest
            @auth
                {{--<li><a href="#">My Contents</a></li>--}}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">@lang('home.greet', ['name' => auth()->user()->name]) <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>@include('partial.logout')</li>
                    </ul>
                </li>
            @endauth
            {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>--}}
                {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="#">Action</a></li>--}}
                    {{--<li><a href="#">Another action</a></li>--}}
                    {{--<li><a href="#">Something else here</a></li>--}}
                    {{--<li><a href="#">Separated link</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>