<?php

namespace App\Console\Commands\Crawl;

use App\Feed;
use App\NewsItem;
use Carbon\Carbon;
use FeedIo\Factory;
use FeedIo\Feed\Item;
use FeedIo\FeedIo;
use Illuminate\Console\Command;

class RssCrawlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl Feeds from news sources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feeds = Feed::get();
        $this->line("Crawling started to ".$feeds->count()." sources");

        foreach($feeds as $feed)
        {
            $this->line("Crawling ".$feed->url." ($feed->channel)");

            $feedio = Factory::create()->getFeedIo();

            $result = $feedio->read($feed->url);

            foreach($result->getFeed() as $item)
            {
                // save news item
                $url = trim($item->getLink());
                $url_hash = md5($url);
                $url_exists = NewsItem::where('url_hash', $url_hash )
                    ->count() > 0;

                if (!$url_exists)
                {
                    $this->info("Saving ".$url);
                    $this->saveFeed($item, $feed->channel);
                }
                else {
                    $this->line("URL ".$url." already exists, skipping...");
                }
            }
        }
    }

    private function saveFeed(Item $feed, $channel = "?")
    {
        $newsItem = new NewsItem();
        $newsItem->type = NewsItem::TYPE_NEWS;
        $newsItem->creator_id = 0;
        $newsItem->source = $channel;
        $newsItem->content_html = (strip_tags(trim($feed->getDescription())));
        $newsItem->author = !is_null($feed->getAuthor()) ? $feed->getAuthor()->getName() : "";
        $newsItem->title = $feed->getTitle();
        $newsItem->url = trim($feed->getLink());
        $newsItem->url_hash = md5(trim($feed->getLink()));

        if ($feed->hasMedia())
        {
            $newsItem->image = $feed->getMedias()[0]->getUrl();
            $newsItem->thumbnail_image = $feed->getMedias()[0]->getUrl();
        }

        if($feed->getLastModified())
        {
            $newsItem->created_at = Carbon::parse($feed->getLastModified()->format("Y-m-d H:i:s"))->toDateTimeString();
        }

        $newsItem->save();
    }

    public static function stripAttributes($html,$attribs) {
//        $dom = new simple_html_dom();
//        $dom->load($html);
//        foreach($attribs as $attrib)
//            foreach($dom->find("*[$attrib]") as $e)
//                $e->$attrib = null;
//        $dom->load($dom->save());
//        return $dom->save();

        return $html;
    }
}
