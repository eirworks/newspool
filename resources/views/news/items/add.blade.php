@extends('layout')

@section('title')
    Add Item
@endsection

@section('content')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('news::add') }}">@yield('title')</a></li>
    </ul>

    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('news::save') }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="">
                </div>

                <div class="form-group">
                    <label>URL</label>
                    <div class="input-group">
                        <div class="input-group-addon">https://</div>
                        <input type="text" class="form-control" name="url" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label>Source</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="source" value="">
                    </div>
                </div>

                <p>
                    <button class="btn btn-primary">Submit</button>
                    <a href="{{ route('home') }}" class="btn btn-link">cancel</a>
                </p>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
@endsection

