<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();
        factory(\App\User::class)->create([
            'email' => 'dev@cc.cc',
            'password' => bcrypt('dev'),
        ]);
        factory(\App\User::class, 10)->create();
    }
}
