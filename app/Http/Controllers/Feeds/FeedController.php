<?php

namespace App\Http\Controllers\Feeds;

use App\Feed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedController extends Controller
{

    const BACKUP_FILE = "feeds.json";

//    public function __construct()
//    {
//        $this->middleware(['auth']);
//    }

    public function feeds(Request $request)
    {
        $feeds = Feed::orderBy('id', 'desc')
            ->paginate(30);

        return view('feeds.feeds', [
            'feeds' => $feeds,
        ]);
    }

    public function newFeed(Request $request)
    {
        $feed = new Feed();

        return view('feeds.feed_form', [
            'feed' => $feed,
        ]);
    }

    public function saveFeed(Request $request)
    {
        $feed = new Feed();

        $feed->url = $request->input('url');
        $feed->name = $request->input('name');
        $feed->channel = $request->input('channel');
        $feed->user_id = auth()->id();

        $feed->save();

        return redirect()->route('feeds::feeds');
    }

    public function deleteFeed(Request $request, $id)
    {
        $feed = Feed::findOrFail($id);

        $feed->delete();

        return redirect()->route('feeds::feeds');
    }

    public function backupFeeds(Request $request)
    {
        $feeds = Feed::get();

        \Storage::put('feeds.json', $feeds->toJson());

        return redirect()->route('feeds::feeds');
    }

    public function restoreFeeds(Request $request)
    {
        if (\Storage::exists(self::BACKUP_FILE))
        {
            Feed::truncate();

            $feeds = json_decode(\Storage::get(self::BACKUP_FILE));

            foreach($feeds as $feed)
            {
                $source = new Feed();
                $source->name = $feed->name;
                $source->url = $feed->url;
                $source->channel = $feed->channel;
                $source->user_id = $feed->user_id;
                $source->fail = $feed->fail;

                $source->save();
            }

            return redirect()->route('feeds::feeds');
        }
    }
}
