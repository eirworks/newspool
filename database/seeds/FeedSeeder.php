<?php

use Illuminate\Database\Seeder;

class FeedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Feed::truncate();
        $feeds = [
            [
                'name' => "BBC Asia News",
                'url' => "http://feeds.bbci.co.uk/news/world/asia/rss.xml",
                'channel' => "BBC",
            ],
            [
                'name' => "BBC Technology",
                'url' => "http://feeds.bbci.co.uk/news/technology/rss.xml",
                'channel' => "BBC",
            ],
            [
                'name' => "BBC Top News",
                'url' => "http://feeds.bbci.co.uk/news/rss.xml",
                'channel' => "BBC",
            ],
        ];

        foreach($feeds as $feed)
        {
            factory(\App\Feed::class)->create($feed);
        }
    }
}
