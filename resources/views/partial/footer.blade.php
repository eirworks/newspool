<p>
    &copy; {{ date("Y") }} {{ env('APP_NAME') }}
</p>
@if(env('APP_DEBUG'))
<p>
    <small>
        Newspool version {{ PROJECT_APP_VERSION }}
        |
        Laravel Version {{ app()->version() }}
        |
        PHP {{ PHP_VERSION }}
    </small>
</p>
@endif