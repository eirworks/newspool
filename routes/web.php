<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/go/{news_item_id}', function ($news_item_id){
    $news_item = \App\NewsItem::find(hashid_decode($news_item_id));
    if (!$news_item)
    {
        return "Item not found...";
    }

    if ($news_item->source == "?" && !request()->has('ok'))
    {
        return view('go_warning', [
            'news_item' => $news_item,
        ]);
    }

    $news_item->increment('visits');

    return view('go', ['news_item' => $news_item]);
})->name('go');

Route::group(['as' => 'news::', 'prefix' => 'news', 'namespace' => 'News' ],function(){
        Route::get('add', "NewsController@addNewsItem")->name('add');
        Route::post('add', "NewsController@saveNewsItem")->name('save');
    });

Route::group(['as' => 'feeds::', 'prefix' => 'feeds', 'namespace' => 'Feeds'], function(){
    Route::get('all', "FeedController@feeds")->name('feeds');
    Route::get('new', "FeedController@newFeed")->name('new');
    Route::post('save', "FeedController@saveFeed")->name('save');
    Route::get('delete/{id}', "FeedController@deleteFeed")->name('delete');
    Route::get('backup', "FeedController@backupFeeds")->name('backup');
    Route::get('restore', "FeedController@restoreFeeds")->name('restore');
    Route::get('view-backup', function(){
        if (\Storage::exists(\App\Http\Controllers\Feeds\FeedController::BACKUP_FILE))
        {
            if (request()->has('download'))
            {
                return response()->download(storage_path('app/'.\App\Http\Controllers\Feeds\FeedController::BACKUP_FILE));
            }
            return response()->file(storage_path('app/'.\App\Http\Controllers\Feeds\FeedController::BACKUP_FILE));
        }
        return response("Backup not found!", 404);
    })->name('backup::view');
});
