@extends('layout')

@section('title')
Search News
@endsection

@section('content')
    <h2 class="text-center">@yield('title') <sup>ALPHA</sup></h2>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="{{ route('home') }}" method="get">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label>Keyword</label>
                    <input type="text" class="form-control" name="q" placeholder="Search Keyword" value="{{ request()->input('q') }}">
                </div>

                <div class="form-group">
                    <label>Content contains</label>
                    <input type="text" class="form-control" name="content" placeholder="Contain in content" value="{{ request()->input('content') }}">
                </div>

                <div class="form-group">
                    <label>Source</label>
                    <input type="text" class="form-control" name="source" placeholder="Source" value="{{ request()->input('source') }}">
                </div>

                <p class="text-center">
                    <button class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                    <button class="btn btn-default" type="reset">Reset</button>
                </p>
            </form>
        </div>
    </div>
@endsection

