<?php

use Faker\Generator as Faker;

$factory->define(\App\Feed::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'url' => $faker->url,
        'channel' => 'News',
        'user_id' => 1,
        'fail' => false,
    ];
});
