<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <title>@yield('title') {{ env('APP_NAME') }}</title>
</head>
<body>
    <header>
        @include('partial.header')
    </header>
    <main class="container">
        @yield('content')
    </main>
    <footer>
        @include('partial.footer')
    </footer>
    <script src="{{ url('js/app.js') }}"></script>
</body>
</html>