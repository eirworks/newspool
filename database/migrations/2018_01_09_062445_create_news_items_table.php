<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('creator_id')->default(0);
            $table->string('title');
            $table->string('author');
            $table->string('source');
            $table->text('url');
            $table->string('url_hash');
            $table->string('image');
            $table->string('thumbnail_image');
            $table->integer('type')->default(0);
            $table->text('content_md');
            $table->text('content_html');
            $table->integer('visits')->default(0);

            $table->index('url_hash');
            $table->index('type');
            $table->index('creator_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_items');
    }
}
