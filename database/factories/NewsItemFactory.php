<?php

use Faker\Generator as Faker;

$factory->define(\App\NewsItem::class, function (Faker $faker) {
    return [
        'creator_id' => 1,
        'title' => $faker->sentence,
        'author' => $faker->name,
        'url' => "http://ddg.gg/?q=".$faker->word,
        'url_hash' => md5($faker->url),
        'image' => "",
        "type" => \App\NewsItem::TYPE_NEWS,
        'content_html' => "",
        'visits' => $faker->numberBetween(99,1000),
    ];
});
