<?php

return [
    'search_news' => "Search News",
    'advanced_search_news' => "Advanced Search",

    'greet' => "Hi, :name",

    'welcome' => "Welcome to :app_name! The trusted news interchange.",
    'read_more_about_us' => "Read more about us",

];