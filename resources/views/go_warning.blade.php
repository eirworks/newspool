@extends('layout')

@section('title', "[Warning] Not reputable news source!")
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-warning text-center">
                <div class="panel-heading">@yield('title')</div>
                <div class="panel-body">
                    <p>
                        The URL <code>{{ $news_item->url }}</code>
                        is not reputable news source!
                    </p>
                    <p class="text-muted">
                        Please be careful out there. A lot of fake news and click bait
                        contents that might lead into fatal misunderstanding and misinformation.
                    </p>
                    <p class="text-center">
                        <a href="{{ route('go',[hashid_encode($news_item->id), 'gb' => str_random(32),'ok' => 1]) }}" class="btn btn-primary btn-lg">I know what I am doing!</a>
                    </p>
                    <p>
                        <small>
                            <a data-toggle="modal" href="#error-in-go">Are you believe this is an error?</a>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="error-in-go">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Is this warning a mistake?</h4>
                </div>
                <div class="modal-body">
                    If you are believe this <code>{{ $news_item->url }}</code> is not a mistake,
                    you may contact us.
                    <div class="form-group">
                        <input type="text" disabled="disabled" value="{{ $news_item->url }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Your Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Your E-mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <textarea name="reason" id="" cols="30" rows="3" placeholder="Reason" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection