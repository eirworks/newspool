<?php

namespace App\Http\Controllers\News;

use App\NewsItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function addNewsItem(Request $request)
    {
        $newsItem = new NewsItem();

        return view('news.items.add', [
            'news_item' => $newsItem,
        ]);
    }

    public function saveNewsItem(Request $request)
    {
        $news_item = new NewsItem();
        $news_item->creator_id = auth()->id();
        $news_item->author = auth()->user()->name;
        $news_item->source = $request->input('source', "");
        $news_item->title = $request->input('title', "");
        $news_item->url = $request->input('url', "");
        $news_item->url_hash = md5($news_item->url);
        $news_item->type = NewsItem::TYPE_NEWS;

        $news_item->save();

        return redirect()->route('home');
    }

    public function deleteNewsItem(Request $request, $id)
    {

    }
}
