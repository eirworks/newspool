<?php

namespace App\Http\Controllers;

use App\Feed;
use App\NewsItem;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news_items = NewsItem::orderBy('created_at', 'desc')
            ->when($request->filled('q'), function($query){
                $query
                    ->where('title', 'like', '%'.\request()->input('q').'%');
            })
            ->when($request->filled('source'), function ($query){
                $query->where('source', 'like', '%'.\request()->input('source').'%');
            })
            ->when($request->filled('content'), function ($query){
                $query->where('content_html', 'like', "%".\request()->input('content')."%");
            })
            ->paginate(30);
        return view('home', [
            'news_items' => $news_items,
            'items' => NewsItem::count(),
            'feeds' => Feed::count(),
        ]);
    }

    public function search(Request $request)
    {
        return view('search');
    }
}
