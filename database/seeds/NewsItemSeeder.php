<?php

use Illuminate\Database\Seeder;

class NewsItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\NewsItem::truncate();
        factory(\App\NewsItem::class, 10)->create();
    }
}
