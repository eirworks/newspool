<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $attributes = [
        'channel' => '?'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
